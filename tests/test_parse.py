"""Test request parsing."""

from jsonrpcobjects.objects import (
    DataError,
    ErrorResponse,
    Notification,
    ParamsNotification,
    ParamsRequest,
    Request,
)
from jsonrpcobjects.parse import parse_request


def test_parse_request() -> None:
    request_str = """{"id": 1, "method": "coffee", "jsonrpc": "2.0"}"""
    request = parse_request(request_str)
    assert isinstance(request, Request)
    assert request.id == 1


def test_parse_params_request() -> None:
    request_str = """{"id": 1, "method": "add", "params": [2, 2], "jsonrpc": "2.0"}"""
    request = parse_request(request_str)
    assert isinstance(request, ParamsRequest)
    assert request.id == 1


def test_parse_notification() -> None:
    request_str = """{"method": "coffee", "jsonrpc": "2.0"}"""
    request = parse_request(request_str)
    assert isinstance(request, Notification)
    assert request.method == "coffee"


def test_parse_params_notification() -> None:
    request_str = """{"method": "add", "params": [2, 2], "jsonrpc": "2.0"}"""
    request = parse_request(request_str)
    assert isinstance(request, ParamsNotification)
    assert request.params == [2, 2]


def test_invalid_json() -> None:
    request_str = """{"method": "add", "params": [2, 2], "jsonrpc": 2.0"}"""
    request = parse_request(request_str)
    assert isinstance(request, ErrorResponse)
    assert request.error.code == -32700


def test_invalid_request() -> None:
    request_str = """{"methods": "add", "params": [2, 2], "jsonrpc": "2.0"}"""
    request = parse_request(request_str)
    assert isinstance(request, ErrorResponse)
    assert request.error.code == -32600
    request_str = "2"
    request = parse_request(request_str)
    assert isinstance(request, ErrorResponse)
    assert request.error.code == -32600


def test_batch() -> None:
    request_str = """{"id": 2, "method": "add", "params": [2, 2], "jsonrpc": "2.0"}"""
    notification_str = """{"method": "add", "params": [2, 2], "jsonrpc": "2.0"}"""
    invalid_req = "2"
    batch_req = f"[{request_str}, {notification_str}, {invalid_req}]"
    request = parse_request(batch_req)
    assert isinstance(request, list)
    assert isinstance(request[0], ParamsRequest)
    assert isinstance(request[1], ParamsNotification)
    assert isinstance(request[2], ErrorResponse)


def test_debug() -> None:
    request_str = """{"method": "add", "params": [2, 2], "jsonrpc": 2.0"}"""
    request = parse_request(request_str, debug=True)
    assert isinstance(request, ErrorResponse)
    assert request.error.code == -32700
    assert isinstance(request.error, DataError)
    assert request.error.data
    request_str = """{"methods": "add", "params": [2, 2], "jsonrpc": "2.0"}"""
    request = parse_request(request_str, debug=True)
    assert isinstance(request, ErrorResponse)
    assert request.error.code == -32600
    assert isinstance(request.error, DataError)
    assert request.error.data
    request_str = "2"
    request = parse_request(request_str, debug=True)
    assert isinstance(request, ErrorResponse)
    assert request.error.code == -32600
    assert isinstance(request.error, DataError)
    assert request.error.data
